package com.deyneka;

public enum Size {
    SMALL,
    MIDDLE,
    LARGE
}
