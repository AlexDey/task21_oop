package com.deyneka;

class Doll extends Toy {

    Doll(int childAge, String name, String make, Size size, int price) {
        super(childAge, name, make, size, price);
    }
}
