package com.deyneka;

abstract class Toy {
    private int childAge;
    private String name;
    private String make;
    private Size size;
    private int price;

    Toy(int childAge, String name, String make, Size size, int price) {
        this.childAge = childAge;
        this.name = name;
        this.make = make;
        this.size = size;
        this.price = price;
    }

    int getChildAge() {
        return childAge;
    }

    void setChildAge(int childAge) {
        this.childAge = childAge;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    String getMake() {
        return make;
    }

    void setMake(String make) {
        this.make = make;
    }

    Size getSize() {
        return size;
    }

    void setSize(Size size) {
        this.size = size;
    }

    int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " " +
                "childAge=" + childAge +
                ", name='" + name + '\'' +
                ", make='" + make + '\'' +
                ", size=" + size +
                ", price=" + price;
    }
}
