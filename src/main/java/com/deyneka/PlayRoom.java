package com.deyneka;

import java.util.*;

class PlayRoom {
    // toys budget limit
    private static final int BUDGET = 1000;
    // the list of toys
    private static List<Toy> toysList = new ArrayList<Toy>();

    // initializes toysList
    PlayRoom() {
        initRoom();
    }

    private void initRoom() {
        List<Toy> temp = new ArrayList<Toy>();
        Toy car1 = new Car(4, "Toyota", "USA", Size.SMALL, 120);
        Toy car2 = new Car(8, "Lada", "Ukraine", Size.MIDDLE, 250);
        Toy doll1 = new Doll(5, "Barbi", "China", Size.SMALL, 200);
        Toy doll2 = new Doll(6, "Ken", "China", Size.SMALL, 210);
        Toy ball1 = new Ball(12, "Basketball", "Poland", Size.LARGE, 140);
        Toy ball2 = new Ball(12, "Football", "Poland", Size.LARGE, 250);
        Collections.addAll(temp, car1, car2, doll1, doll2, ball1, ball2);

        // limits toys quantity
        int count = 0;
        for (Toy toy : temp) {
            count += toy.getPrice();
            if (count <= BUDGET) {
                toysList.add(toy);
            }
        }
    }

    void sortByPrice() {
        toysList.sort(Comparator.comparing(Toy::getPrice));
        System.out.println("Collection sorted by price:");
        toysList.forEach(System.out::println);
    }

    void sortBySize() {
        toysList.sort(Comparator.comparing(Toy::getSize));
        System.out.println("Collection sorted by size:");
        toysList.forEach(System.out::println);
    }

    void findByMake(String country) {
        System.out.println("Search results:");
        for (Toy toy : toysList) {
            if (country.equals(toy.getMake())) {
                System.out.println(toy);
            }
        }
    }

    // forms playroom for children
    void findByChildAge(int lowerLimit, int higherLimit) {

        System.out.println("Playroom for " + lowerLimit + "-" + higherLimit + " years children:");
        for (Toy toy : toysList) {
            if (toy.getChildAge()>= lowerLimit & toy.getChildAge()<= higherLimit) {
                System.out.println(toy);
            }
        }
    }
}
